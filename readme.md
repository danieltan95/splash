Splash
============

[Unsplash.it](https://unsplash.it/) CLI tool. It gets a picture from the site and sets it as the desktop background.

Build
------------

It's written in D, so you'll need a D compiler. You can grab it from https://dlang.org/download.html .

`dub build` and you're all set.

Usage
------------

If the compiled program is called without any options, then it will download a random picture and save it to your `$HOME/Pictures/splash` folder before setting it as the desktop background.

The program will automatically discover your screensize and your windows manager so you don't have to configure that.

`-f` or `--file` : path to your image (it can contain a tilde)  
`-i` or `--id` : id of image found in https://unsplash.it/images  
`-d` or `--dir` : directory to save the image to (it can contain a tilde)  

Terminal.d
============

This is modified from https://github.com/adamdruppe/arsd/blob/master/terminal.d

I've changed it to use delimited strings, and also exposed the update cursor position function instead of using linegetter. 