auto get_monitor_size(){
    import std.process, std.array;
    return executeShell("xdpyinfo | grep 'dimensions'").output.split()[1].split("x");
}

string construct_unsplash_it_url(string[] dimensions){
    return "https://unsplash.it/"~dimensions[0]~"/"~dimensions[1]~"/?";
}

string generate_random_picture_name(string path){
    import std.path, std.uuid, std.file, std.conv;
    path=expandTilde(path);
    if(!exists(path)){
        mkdir(path);
    }
    return buildPath(path, randomUUID().to!(string))~".jpg";
}

int convert_percent(float current, float total){
    import std.conv, std.math;
    if(total==0) return 0;
    return ((current/total)*10F).floor.to!int;
}

void download(string url, string path){
    // Track progress
    import std.net.curl, std.outbuffer, std.file, std.stdio, std.array, terminal;
    OutBuffer b = new OutBuffer();
    auto term = Terminal(ConsoleOutputType.linear);
    term.autoHideCursor();
    auto http = HTTP(url);
    http.method = HTTP.Method.get;
    http.onReceive = (ubyte[] data) { 
        b.write(data);
        return (data.length); 
    };
    auto currentX=term.cursorX;
    auto currentY=term.cursorY;        
    spinner s= new spinner();
    http.onProgress = (size_t dltotal, size_t dlnow,
                       size_t ultotal, size_t ulnow)
    {
        term.moveTo(currentX, currentY, ForceOption.alwaysSend);
        int progress=convert_percent(dlnow,dltotal);
        term.color(Color.DEFAULT, Color.DEFAULT);
        term.write("[");
        term.color(Color.green,Color.DEFAULT);
        term.write("#".replicate(progress));
        term.color(Color.red,Color.DEFAULT);
        term.write("-".replicate(10-progress));
        term.color(Color.DEFAULT, Color.DEFAULT);
        term.write("](");
        term.color(Color.yellow,Color.DEFAULT);
        term.write(s.spin());
        term.color(Color.DEFAULT, Color.DEFAULT);
        term.write(")");
        term.flush();
        term.moveTo(currentX, currentY, ForceOption.alwaysSend);
        return 0;
    };
    http.perform();    
    writeln();
    std.file.write(path, b.toBytes());
}

void set_desktop_background(string path){
    import std.process, std.path, std.stdio, std.uni;
    string desktop = environment.get("XDG_CURRENT_DESKTOP").toLower();
    string session = environment.get("GDMSESSION").toLower();
    if(desktop=="pantheon" || desktop=="gnome" || desktop=="unity"){
        executeShell("gsettings set org.gnome.desktop.background picture-uri file:///"~absolutePath(path));
    }else if(desktop=="xfce"){
        executeShell("xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitor0/image-path --set "~absolutePath(path));
    }else if(desktop=="x-cinnamon"){
        executeShell("gsettings set org.cinnamon.desktop.background picture-uri file:///"~absolutePath(path));
    }else{
        writeln("Your windows manager is not supported.");
    }
}

class spinner{
    int count=-1;
    string spin(){
        count++;
        return["-","\\","|","/"][count%4];
    }
}

void main(string[] args)
{
    import std.stdio, std.file, std.getopt;
    string file="";
    string id="";
    string dir="";
    auto info = getopt(args, 
        "file|f", "path to your image", &file, 
        "id|i", "id of image found in https://unsplash.it/images", &id,
        "dir|d", "directory to save to", &dir);
    if (info.helpWanted){
        defaultGetoptPrinter("Unsplash.it CLI program.", info.options);
        return;
    }
    string baseurl=construct_unsplash_it_url(get_monitor_size());
    string path=generate_random_picture_name(dir=="" ? "~/Pictures/splash" : dir);
    if(file!="" && id!=""){
        writeln("Either use -f or -i.");
        return;
    }else if(id!=""){
        download(baseurl~"image="~id, path);
    }else if(file!=""){
        path=file;
    }else{
        download(baseurl~"random", path);
    }
    set_desktop_background(path);
}
